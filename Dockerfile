FROM alpine AS build
ENV KUBELOGIN_DOWNLOAD_SHA256 10e9a06bd0f18f8252875a9dae626a0098cc7ba708695104442be36a836b6870
RUN apk add --no-cache curl unzip \
  && curl https://github.com/Azure/kubelogin/releases/download/v0.0.6/kubelogin-linux-amd64.zip -L -o kubelogin.zip \
  && unzip -o kubelogin.zip \
  && echo "$KUBELOGIN_DOWNLOAD_SHA256 *kubelogin.zip" | sha256sum -c - \
  && mv bin/linux_amd64/kubelogin /usr/local/bin \
  && chmod 755 /usr/local/bin/kubelogin

FROM mspipes/azure-aks-deploy:1.0.2
LABEL maintainer="g.fritz@landbellgroup.com"
LABEL org.label-schema.schema-version="0.1"
LABEL org.label-schema.build-cmd="docker build --build-arg BUILD_DATE=$BUILD_DATE --build-arg BITBUCKET_BUILD_NUMBER=$BITBUCKET_BUILD_NUMBER --build-arg VERSION_TAG=$VERSION_TAG --build-arg DOCKER_IMAGE_NAME=$DOCKER_IMAGE_NAME ."
LABEL org.label-schema.build-date="$BUILD_DATE"
LABEL org.label-schema.build-number="$BITBUCKET_BUILD_NUMBER"
LABEL org.label-schema.name="circul8/azure-aks-deploy"
LABEL org.label-schema.description="AKS 'kubectl' Pipe for usage as Pipe in Bitbucket Pipelines"
LABEL org.label-schema.url="https://circul8.world/"
LABEL org.label-schema.vcs-url="https://bitbucket.org/$BITBUCKET_REPO_FULL_NAME"
LABEL org.label-schema.vcs-ref="$VCS_REF"
LABEL org.label-schema.vendor="Landbell AG fuer Rueckholsysteme"
LABEL org.label-schema.version="$VERSION_TAG-$BITBUCKET_BUILD_NUMBER"
LABEL org.label-schema.docker.cmd="docker run -e AZURE_APP_ID=... -e AZURE_PASSWORD=... -e AZURE_TENANT_ID=... -e AZURE_AKS_NAME=... -e AZURE_RESOURCE_GROUP=... -e KUBECTL_COMMAND=version -e KUBECTL_ARGUMENTS=optional -e KUBERNETES_SPEC_FILE=optional -e DEBUG=true $DOCKER_IMAGE_NAME"
COPY --from=build /usr/local/bin/kubelogin /usr/bin/
ADD pipe/ /
ENTRYPOINT ["/pipe.sh"]
